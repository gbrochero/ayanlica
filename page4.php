<html>
<head>
    <meta charset="utf-8">

<title>Radianes-Grados</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="icon" type="image/x-icon" href="img/logo.ico">
    <link rel="stylesheet" type="text/css" href="css/csspage/estilopage4.css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  
  $(function() {
  
  // elementos de la lista
  var menues = $(".nav li"); 

  // manejador de click sobre todos los elementos
  menues.click(function() {
     // eliminamos active de todos los elementos
     menues.removeClass("activo");
     // activamos el elemento clicado.
     $(this).addClass("activo");
  });

});
</script>
</head>
 
<body>
<header> 
        <nav>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="page1.php">Grados-Fahrenheit</a></li>
                <li class="activo"><a href="page4.php">Radianes-Grados</a></li>
                <li><a href="page3.php">Raiz Cuadrada</a></li>
                <li><a href="fracciones.php">Multi-Fracciones</a></li>
                <li><a href="page2.php">Area del Cuadrado</a></li>
                <li><a href="velocidad.php">Velocidad</a></li>
                </ul>
        </nav>
    </header>
<br><br>
<div id="container">
 <div class="container-fluid btn-success">
  <h1 class=" container ">Grados-Radianes y Radianes-Grados</h1>
  </div>
  <center>
<h1 id="solicitud">Convertir de radianes a grados 
<br>
<h2 id="solicitud">Ingrese el numero a convertir</h2>
<form action="#" method="POST">

<input type="number" name  ="numero1" placeholder="Digito a convertir" required>
<br>
<br>
<input type="submit" name  ="op" value = "Pasar a Grados" class="btn btn-success">

<input type="submit" name  ="op"  value = "Pasar a Radian" class="btn btn-success">
</form>
<?php
if($_POST){
 if ($_POST['op']=="Pasar a Grados")
  {
    $rd = $_POST ['numero1']; 

echo rad2deg($rd);
  }

else if ($_POST['op']=="Pasar a Radian") 
  {
    $dg = $_POST ['numero1']; 

echo deg2rad($dg);
}
}
?>
</center>
</div>
<footer>
      <div id="subfooter">
          <div id="contac">
              <h2>Contacto</h2>
              <p>Direccion: Calle Manhattan 54-53</p>
              <p>tlf: 301-9857789</p>

          </div>
          <div id="redes">
               <div id="facebook"><img class="imgr" src="img/face.png"></div>
               <div id="instagram"><img class="imgr" src="img/insta.png"></div>
               <div id="gmail"><img class="imgr" src="img/gmail.png"></div>
          </div>
      </div>
      <div id="derecho">
          <center><p>Ayanlica © | 2017</p></center>
      </div>
  </footer>
</body>

</html>