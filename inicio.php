<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Ayanlica</title>
  <meta name="keywords" content="raizcuadrada,raiz cuadrada,areadeuncuadrado,area de un cuadrado,fraccion,velocidad,grados,fahrenheit,radianes,operaciones,operacion,ejercicios,ayanlica,gianlucabrochero,andreaarevalo,leonardorincon,jorgeduarte,angelicacamargo,sena,adsi,44,php,prueba,actividad"> 
  <link rel="icon" type="image/x-icon" href="img/logo.ico">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/font-icon.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/csspage/estiloinicio.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
      $("#loadpage").delay(200).fadeOut("slow");
    });

  </script>
</head>
<body>
      <center><span id="loadpage"><img id="loadf" src="img/cargando.gif"></span></center>

  <center><div id="enca">
  <h1 id="titulo">Elija un Opción</h1>
  </div></center>
<section id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 portfolio-item"> <a href="page1.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Cambio de Grados a Fahrenheit</h3>
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imga1.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="page4.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Radianes a Grados y viceversa</h3>
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imag2.jpg" class="img-responsive" alt=""> </a></div>
      <div class="col-sm-6 portfolio-item"> <a href="page3.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Raiz Cuadrada</h3>
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imag3.jpg" class="img-responsive" alt=""> </a></div>
      <div id ="cuadro" class="col-sm-6 portfolio-item"> <a href="fracciones.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Multiplicacion de dos fracciones</h3> 
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imag4.jpg" class="img-responsive" alt=""> </a></div>
         <div class="col-sm-6 portfolio-item"> <a href="page2.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Area de un Cuadro</h3>
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imag6.jpg" class="img-responsive" alt=""> </a> </div>
         <div class="col-sm-6 portfolio-item"> <a href="velocidad.php" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>Velocidad</h3>
            <h4>Ayanlica</h4>
          </div>
        </div>
        <img src="images/portfolio/imag5.jpg" class="img-responsive" alt=""> </a> </div>
       </div>
  </div>
</section>
</body>
</html>