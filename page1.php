<html>
    <head>
        <meta charset="utf-8">

        <title>Grados-Fahrenheit</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="icon" type="image/x-icon" href="img/logo.ico">
        <link rel="stylesheet" type="text/css" href="css/csspage/estilopage1.css">
        <meta name="viewport" content="width=device-width, user-scalable=no,        initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  
  $(function() {
  
  // elementos de la lista
  var menues = $(".nav li"); 

  // manejador de click sobre todos los elementos
  menues.click(function() {
     // eliminamos active de todos los elementos
     menues.removeClass("activo");
     // activamos el elemento clicado.
     $(this).addClass("activo");
  });

});
</script>
    </head>
<body>
    <header> 
        <nav>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li class="activo"><a href="page1.php">Grados-Fahrenheit</a></li>
                <li><a href="page4.php">Radianes-Grados</a></li>
                <li><a href="page3.php">Raiz Cuadrada</a></li>
                <li><a href="fracciones.php">Multi-Fracciones</a></li>
                <li><a href="page2.php">Area del Cuadrado</a></li>
                <li><a href="velocidad.php">Velocidad</a></li>
              </ul>
        </nav>
    </header>
    <br><br>
  <div id="container">
    <div class="container-fluid btn-info">
  <h1 class=" container-fluid ">Grados a Fahrenheit</h1>
  </div>
 <center>

    <form method="POST" action="page1.php"> 
    <br>

    <h1 id="solicitud">Ingrese el valor de grados centigrados</h1>
    <br> 

   <input  type="number" name="xss" placeholder="Value" required >
    <br> 
    <br>
    <input id="bt" type="submit" name="enviar" value="Consultar" class="btn btn-info">

    </form> 
    <?php
    if($_POST){
        if(isset ($_POST["enviar"])){
            $value = $_POST["xss"];
        if(($value) == ""){
            ?>
            <div class="alert alert-danger">No puede dejar el campo vacio</div>

    <?php   
        }
        }
    }
	?>
    
        <?php
    if($_POST){
        if(isset ($_POST["enviar"])){
            $value = $_POST["xss"];
         if(($value) != ""){
            $calc = $value * (9);
            $fn = ($calc / 5)+32;
             ?>
    <div class="alert alert-info">El resultado de la operacion es:
<?php
            echo " ".$fn ." °F";
             ?>
             </div>
    <?php
        }
        }
    }
	?>
  </center>
  </div>
<footer>
      <div id="subfooter">
          <div id="contac">
              <h2>Contacto</h2>
              <p>Direccion: Calle Manhattan 54-53</p>
              <p>tlf: 301-9857789</p>

          </div>
          <div id="redes">
               <div id="facebook"><img class="imgr" src="img/face.png"></div>
               <div id="instagram"><img class="imgr" src="img/insta.png"></div>
               <div id="gmail"><img class="imgr" src="img/gmail.png"></div>
          </div>
      </div>
      <div id="derecho">
          <center><p>Ayanlica © | 2017</p></center>
      </div>
  </footer>
</body>
</html>

   