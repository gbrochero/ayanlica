<html>
    <head>
        <meta charset="utf-8">

        <title>Area de un Cuadrado</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="icon" type="image/x-icon" href="img/logo.ico">
        <link rel="stylesheet" type="text/css" href="css/csspage/estilopage2.css">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  
  $(function() {
  
  // elementos de la lista
  var menues = $(".nav li"); 

  // manejador de click sobre todos los elementos
  menues.click(function() {
     // eliminamos active de todos los elementos
     menues.removeClass("activo");
     // activamos el elemento clicado.
     $(this).addClass("activo");
  });

});
</script>

    </head>
<body>
    <header> 
        <nav>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="page1.php">Grados-Fahrenheit</a></li>
                <li><a href="page4.php">Radianes-Grados</a></li>
                <li><a href="page3.php">Raiz Cuadrada</a></li>
                <li><a href="fracciones.php">Multi-Fracciones</a></li>
                <li class="activo"><a href="page2.php">Area del Cuadrado</a></li>
                <li><a href="velocidad.php">Velocidad</a></li>
                </ul>
        </nav>
    </header>
    <br><br>
    <div id="container">
    <div class="container-fluid btn-primary">
  <h1 class=" container ">Area de un cuadrado</h1>
  </div>
<center>   
    <form method="POST" action="page2.php"> 
    <h1 id="solicitud">Ingrese los valores de los lados de los cuadrados</h1>
    <br> 
    <input type="number" name="lado1" placeholder="Primer lado" required >
    <br> 
    <br>
    <input type="number" name="lado2" placeholder="Segundo lado" required >
    <br>
    <br>
    <input type="submit" name="calcular" value="Consultar" class="btn btn-primary">

    </form>
     <?php
    if($_POST){
        if(isset ($_POST["calcular"])){
            $value1 = $_POST["lado1"];
            $value2 = $_POST["lado2"];
        if(($value1 && $value2) == ""){
            ?>
            <div class="alert alert-danger">No puede dejar el campo vacio o igual a 0</div>

    <?php
            }
         }
     }  
     if($_POST){
         if(isset ($_POST["calcular"])){
             $value1 = $_POST["lado1"];
             $value2 = $_POST["lado2"];
        if(($value1 && $value2) != ""){
            $result = $value1 * $value2;
?>
    <div class="alert alert-info">El resultado de la operacion es:
<?php
            echo " ".$result ." metros²";
             ?>
             </div>
    <?php
     
    
       
        }
        }
    }
	?>    
  </center>
  </div>
  <footer>
      <div id="subfooter">
          <div id="contac">
              <h2>Contacto</h2>
              <p>Direccion: Calle Manhattan 54-53</p>
              <p>tlf: 301-9857789</p>

          </div>
          <div id="redes">
               <div id="facebook"><img class="imgr" src="img/face.png"></div>
               <div id="instagram"><img class="imgr" src="img/insta.png"></div>
               <div id="gmail"><img class="imgr" src="img/gmail.png"></div>
          </div>
      </div>
      <div id="derecho">
          <center><p>Ayanlica © | 2017</p></center>
      </div>
  </footer>
</body>
</html>
