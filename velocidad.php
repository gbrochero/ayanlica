<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Velocidad</title>
<link rel="stylesheet" type="text/css" href="css/csspage/estilovelocidad.css">
	    <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="icon" type="image/x-icon" href="img/logo.ico">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  
  $(function() {
  
  // elementos de la lista
  var menues = $(".nav li"); 

  // manejador de click sobre todos los elementos
  menues.click(function() {
     // eliminamos active de todos los elementos
     menues.removeClass("activo");
     // activamos el elemento clicado.
     $(this).addClass("activo");
  });

});
</script>
</head>
<body>
	 <header> 
        <nav>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="page1.php">Grados-Fahrenheit</a></li>
                <li><a href="page4.php">Radianes-Grados</a></li>
                <li><a href="page3.php">Raiz Cuadrada</a></li>
                <li><a href="fracciones.php">Multi-Fracciones</a></li>
                <li><a href="page2.php">Area del Cuadrado</a></li>
                <li class="activo"><a href="velocidad.php">Velocidad</a></li>
                </ul>
        </nav>
    </header><br><br>
    <div id="container">
 <div class="container-fluid btn-warning">
  <h1 class=" container ">Velocidad</h1>
  </div>
<center>
	<form action = "velocidad.php" method = "POST">
<h1 id="solicitud">Ingrese la distancia recorrida</h1>
 <input type="number" name="distancia" placeholder="Distancia" required> (Kilometros)
<h1 id="solicitud">Ingrese el tiempo de recorrido</h1>
<input type="number" name="tiempo" placeholder="Tiempo" required> (Horas)</br>

<?php

if($_POST)
{
$distancia1 = $_POST ['distancia'];
$tiempo1 = $_POST ['tiempo'];
$velocida = $distancia1/$tiempo1;

}

?>
<h3>La velocidad es </h3><input type="number" disabled value="<?php echo $velocida; ?>" >

<h2><input type="submit" name="op" value="Calcular" class="btn btn-warning"> </h2>

</form>
</center>
</div>
<footer>
      <div id="subfooter">
          <div id="contac">
              <h2>Contacto</h2>
              <p>Direccion: Calle Manhattan 54-53</p>
              <p>tlf: 301-9857789</p>

          </div>
         <div id="redes">
               <div id="facebook"><img class="imgr" src="img/face.png"></div>
               <div id="instagram"><img class="imgr" src="img/insta.png"></div>
               <div id="gmail"><img class="imgr" src="img/gmail.png"></div>
          </div>
      </div>
      <div id="derecho">
          <center><p>Ayanlica © | 2017</p></center>
      </div>
    </footer>
</body>
</html>