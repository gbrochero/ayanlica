<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">

	<title>Ayanlica</title>
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="icon" type="image/x-icon" href="img/logo.ico">
 <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="js/com.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Gravitas+One" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
			$("#loadpage").delay(500).fadeOut("slow");
		});

	</script>
	<script type="text/javascript">
		
		window.addEventListener("orientationchange", ()=> {
    console.log(window.screen.orientation);
});

if (window.matchMedia("(orientation: portrait)").matches) {
  $(document).on('ready', function () {
  	 $('#orientacion').css('display', 'block');

	})
}

if (window.matchMedia("(orientation: landscape)").matches) {
	$(document).on('ready', function () {
 $('#orientacion').css('display', 'none');
	})
}
	</script>
</head>
<body>
	<center><span id="orientacion"><img id=="imgorientacion" src="img/orientacion.gif"><br><br><h4>Para mayor diversion coloque su dispositivo horizontalmente y vuelva a cargar la pagina</h4></span></center>
		<center><span id="loadpage"><img id="loadf" src="img/cargando.gif"></span></center>

	<div id="container">
		<header>
			
		<div class="menu_bar">
			<img id="logo1" src="img/logo.png">
				<a href="login.php"><img id="logo2" src="img/ilg.png" alt="iniciar sesion"></a>
			<div class="bt-menu"><img id="rayas" src="img/menu.png"></div>
		</div>
 
		<nav>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="quienessomos.php">Quienes Somos</a></li>
				<li><a href="inicio.php">Empieza la Diversion</a></li>
				<li><a href="contacto.php">Contacto</a></li>
				</ul>
		</nav>
	</header>	
	<div id="main">
	<div id="main1">
		<center><h1 id="titulo">AYANLICA</h1></center>
	</div>
	<div id="cuadroq">
			<div id="info">
					<h1 id="mision">Misión</h1>
					<p>Facilitar al usuario realizar <span>operaciones matemáticas predeterminadas</span> a través de una plataforma web intuitiva e interactiva.
					</p>
					<h1 id="vision">Visión</h1>
					<p>Ser lideres en latinoamérica entre las distintas plataformas que brindan servicios de operaciones matematicas.
					</p>
					<h1 id="grupodetrabajo">Grupo de Trabajo</h1>

					<div id="grupo">
					<div id="andrea"><img id="andreaf" src="img/andrea.png"></div>
					<div id="angelica"><img  id="angelicaf" src="img/angelica.png"></div>
					<div id="leonardo"><img  id="leonardof" src="img/leonardo.png"></div>
					<div id="duarte"><img  id="duartef" src="img/duarte.png"></div>
					<div id="gianluca"><img  id="gianlucaf" src="img/gianluca.png"></div>

					</div>
			</div>
		</div>
	</div>
	
	</div>
	</div>
</body>
</html>