<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

	<title>Multiplicacion de Fracciones</title>
  <link rel="icon" type="image/x-icon" href="img/logo.ico">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

  <link rel="stylesheet" href="css/estilos1.css">
  <link rel="stylesheet" type="text/css" href="css/csspage/estilofracciones.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
  
  $(function() {
  
  // elementos de la lista
  var menues = $(".nav li"); 

  // manejador de click sobre todos los elementos
  menues.click(function() {
     // eliminamos active de todos los elementos
     menues.removeClass("activo");
     // activamos el elemento clicado.
     $(this).addClass("activo");
  });

});
</script>

</head>
<body>
  <header> 
        <nav>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="page1.php">Grados-Fahrenheit</a></li>
                <li><a href="page4.php">Radianes-Grados</a></li>
                <li><a href="page3.php">Raiz Cuadrada</a></li>
                <li class="activo"><a href="fracciones.php">Multi-Fracciones</a></li>
                <li><a href="page2.php">Area del Cuadrado</a></li>
                <li><a href="velocidad.php">Velocidad</a></li>
                </ul>
        </nav>
    </header>
    <br><br>
    <center>
    <div id="container">
  <div class="container-fluid btn-danger">
  <h1 class=" container ">Multiplicacion de Fracciones</h1>
  </div>
  <form class="container" action="#" method="post">
    <div id="con">
     <div class="row" id="fraccion1">
        <div class="col-xs-2">
            <input type="text" class="form-control" name="numero1" placeholder="Numerador" required><hr>
            <input type="text" class="form-control" name="numero2" placeholder="Denominador" required> 
       </div>
            <div id="signox"><h5>X</h5></div>
  </div>

    <div class="row" id="fraccion2">
          <div class="col-xs-2 ">
            <input type="text" class="form-control" name="numero3" placeholder="Numerador" required><hr>
            <input type="text" class="form-control" name="numero4" placeholder="Denominador" required>
          </div>
            <div id="signoi"><h5>=</h5></div>
    </div>

</div>
<button class="btn btn-danger" id="boton3">Calcular</button>
<?php  
 $r1="";
  $r2="";
if ($_POST) {

  $num1=$_POST['numero1'];
  $num2=$_POST['numero2'];
  $num3=$_POST['numero3'];
  $num4=$_POST['numero4'];
  $r1=$num1*$num4;
  $r2=$num2*$num3;
}

?>
<div class="row" id="fraccion3">
          <div class="col-xs-2 ">
            <input type="text" class="form-control" name="respuesta1" placeholder="Numerador" disabled value="<?php echo $r1; ?>"><hr>
            <input type="text" class="form-control" name="respuesta2" placeholder="Denominador" disabled value="<?php echo $r2; ?>">
    
          </div>
  </div>
</form>
</center>
</div>
<footer>
      <div id="subfooter">
          <div id="contac">
              <h2>Contacto</h2>
              <p>Direccion: Calle Manhattan 54-53</p>
              <p>tlf: 301-9857789</p>

          </div>
          <div id="redes">
               <div id="facebook"><img class="imgr" src="img/face.png"></div>
               <div id="instagram"><img class="imgr" src="img/insta.png"></div>
               <div id="gmail"><img class="imgr" src="img/gmail.png"></div>
          </div>
      </div>
      <div id="derecho">
          <center><p>Ayanlica © | 2017</p></center>
      </div>
</footer>
</body>

</html>
